import React, {Component} from 'react';
import { compose } from 'redux';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';

import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import styles from './styles';
import Logo from 'shareAssets/logo.png';


class Login extends Component {
    ImportPriKey = () => {
        return (
            <Typography variant="body2" color="textSecondary" align="center">
                {'Import Private Key'}
            </Typography>
        );
    };

    render() {
        const { classes } = this.props;
        return (
            <Grid container component="main" className={classes.root}>
                <CssBaseline />
                <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                <div className={classes.paper}>
                    <img src={Logo} className={classes.logo} alt="uGroop logo" />
                <Typography component="h1" variant="h5" className={classes.subLogoTitle}>
                    Your Digital Wallet
                </Typography>
                <form className={classes.form} noValidate>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        placeholder="Email address"
                        id="email"
                        name="email"
                        autoComplete="email"
                        autoFocus
                        InputProps={{
                            classes: {
                                notchedOutline: classes.notchedOutline
                            }
                        }}
                    />
                    <div style={{position:'relative'}}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        placeholder="Password"
                        floatingLabelText="forgot"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        InputProps={{
                            classes: {
                                notchedOutline: classes.notchedOutline
                            }
                        }}
                    />
                        <Link href="#" variant="body2" style={{position:'absolute', top:35, right: 25, color:'#3d3d3d'}}>
                            {"Forgot?"}
                        </Link>
                    </div>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Sign In
                    </Button>
                    {/*<Grid container>*/}
                    {/*    <Grid item xs>*/}
                    {/*        <Link href="#" variant="body2">*/}
                    {/*            Forgot password?*/}
                    {/*        </Link>*/}
                    {/*    </Grid>*/}
                    {/*    <Grid item>*/}
                    {/*        <Link href="#" variant="body2">*/}
                    {/*            {"Don't have an account? Sign Up"}*/}
                    {/*        </Link>*/}
                    {/*    </Grid>*/}
                    {/*</Grid>*/}
                    <Box mt={5}>
                        { this.ImportPriKey() }
                    </Box>
                </form>
            </div>
            </Grid>
                <Grid item xs={false} sm={4} md={7} className={classes.image} />
            </Grid>
        );
    }
}

Login.propTypes = {
    // hoc props
    classes: PropTypes.object.isRequired,
};

export default compose(
    withStyles(styles,{name:'Login'})
)(Login);
