import AvatarBG from 'shareAssets/mask-group-2x.png';

const styles = theme => ({
    root: {
        height: '100vh',
    },
    image: {
        backgroundImage: `url(${AvatarBG})`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundSize: 'cover',
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        paddingTop: '60px',
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
        backgroundColor: '#c6b5a5 !important',
        paddingTop: '10px',
        paddingBottom: '10px',
    },
    logo: {
        marginTop: '180px',
        width: '200px',
    },
    subLogoTitle: {
        marginTop: '5px',
    },
    notchedOutline: {
        borderWidth: "1px",
        borderColor: '#c6b5a5 !important',
        '&:hover': {
            borderColor: '#c6b5a5 !important',
        },
        '&:active, &:focus': {
            borderColor: '#c6b5a5 !important',
        },
    }
});

export default styles;
